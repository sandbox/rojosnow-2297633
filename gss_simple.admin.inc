<?php

/**
 * @file
 * Admin page callbacks for the gss_simple module.
 */

/**
 * Admin settings page for gss_simple.
 */
function gss_simple_admin_settings() {
  $form = array();
  $form['gss_simple'] = array(
    '#title' => t('Google Site Search Simple'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['gss_simple']['general'] = array(
    '#title' => t('General settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['gss_simple']['general']['gss_simple_cx'] = array(
    '#title' => t('Google search engine ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('gss_simple_cx', ''),
    '#description' => t('Enter your Google search engine ID (<a target="_blank" href="http://www.google.com/cse/manage/all">Google CSE control panel</a>).'),
  );
  $form['gss_simple']['general']['gss_simple_path'] = array(
    '#title' => t('Path name of the Google Site Search'),
    '#type' => 'textfield',
    '#maxlength' => 50,
    '#size' => 60,
    '#description' => t('Enter a local path name of the site search (defaults to %path).', array('%path' => t('site'))),
    '#default_value' => variable_get('gss_simple_path', 'site'),
  );
  $form['gss_simple']['general']['gss_simple_results_tab'] = array(
    '#title' => t('Search results tab name'),
    '#type' => 'textfield',
    '#maxlength' => 50,
    '#size' => 60,
    '#description' => t('Enter a custom name of the tab where search results are displayed (defaults to %tab).', array('%tab' => t('Site Search'))),
    '#default_value' => variable_get('gss_simple_results_tab', 'Site Search'),
  );

  $form['gss_simple']['search_form'] = array(
    '#title' => t('Search form settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['gss_simple']['search_form']['gss_simple_enable_search_form_history'] = array(
    '#title' => t('Enable browser history on the search form?'),
    '#type' => 'checkbox',
    '#description' => t('If checked, enables history management for the browser Back and Forward buttons when using the search form.'),
    '#default_value' => variable_get('gss_simple_enable_search_form_history', TRUE),
  );
  $form['gss_simple']['search_form']['gss_simple_enable_search_form_auto'] = array(
    '#title' => t('Enable autocomplete on the search form?'),
    '#type' => 'checkbox',
    '#description' => t('If checked, autocomplete will be enabled on the search form. Please note, autocomplete must be enabled on Google Site Search.'),
    '#default_value' => variable_get('gss_simple_enable_search_form_auto', TRUE),
  );
  $form['gss_simple']['search_form']['gss_simple_search_form_class'] = array(
    '#title' => t('Search form class name'),
    '#type' => 'textfield',
    '#size' => 60,
    '#description' => t('Enter a class name for the search form (defaults to %css).', array('%css' => t('gss-search-form'))),
    '#default_value' => variable_get('gss_simple_search_form_class', 'gss-search-form'),
  );

  $form['gss_simple']['search_block_form'] = array(
    '#title' => t('Search block form settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['gss_simple']['search_block_form']['gss_simple_enable_search_block_form'] = array(
    '#title' => t('Enable the search block form when displaying search results?'),
    '#type' => 'checkbox',
    '#description' => t('If checked, the search block form will be enabled when search results are displayed.'),
    '#default_value' => variable_get('gss_simple_enable_search_block_form', TRUE),
  );
  $form['gss_simple']['search_block_form']['gss_simple_enable_search_block_form_auto'] = array(
    '#title' => t('Enable autocomplete on the search block form?'),
    '#type' => 'checkbox',
    '#description' => t('If checked, autocomplete will be enabled on the search block form. Please note, autocomplete must be enabled on Google CSE.'),
    '#default_value' => variable_get('gss_simple_enable_search_block_form_auto', TRUE),
  );
  $form['gss_simple']['search_block_form']['gss_simple_search_block_form_class'] = array(
    '#title' => t('Search block form class name'),
    '#type' => 'textfield',
    '#size' => 60,
    '#description' => t('Enter a class name for the search block form (defaults to %css).', array('%css' => t('gss-search-block-form'))),
    '#default_value' => variable_get('gss_simple_search_block_form_class', 'gss-search-block-form'),
  );

  $form['gss_simple']['search_results'] = array(
    '#title' => t('Search results settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['gss_simple']['search_results']['gss_simple_search_results_class'] = array(
    '#title' => t('Search results class name'),
    '#type' => 'textfield',
    '#size' => 60,
    '#description' => t('Enter a class name for the search results (defaults to %css).', array('%css' => t('gss-search-results'))),
    '#default_value' => variable_get('gss_simple_search_results_class', 'gss-search-results'),
  );

  return $form;
}
