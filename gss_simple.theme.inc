<?php

/**
 * @file
 * Themeable functions for Google Site Search Simple.
 */

/**
 * The search form can be themed/customized.
 */
function template_preprocess_gss_simple_search_form(&$variables) {
  // @link https://developers.google.com/custom-search/docs/element#supported_attributes
  $variables['search_form_class'] = variable_get('gss_simple_search_form_class', 'gss-search-form');
  $variables['enable_search_form_history'] = variable_get('gss_simple_enable_search_form_history', TRUE);
  $variables['enable_search_form_auto'] = variable_get('gss_simple_enable_search_form_auto', TRUE);
}

/**
 * The search block form can be themed/customized.
 */
function template_preprocess_gss_simple_search_block_form(&$variables) {
  $variables['search_block_form_class'] = variable_get('gss_simple_search_block_form_class', 'gss-search-block-form');
  $variables['results_url'] = '/search/' . variable_get('gss_simple_path', 'site');
  $variables['enable_search_block_form_auto'] = variable_get('gss_simple_enable_search_block_form_auto', TRUE);
}

/**
 * The search results can be themed/customized.
 */
function template_preprocess_gss_simple_search_results(&$variables) {
  $variables['search_results_class'] = variable_get('gss_simple_search_results_class', 'gss-search-results');
}
