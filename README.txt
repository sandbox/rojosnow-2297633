
-- SUMMARY --

Google has two levels of Custom Search to provide local search for a website:
Custom Search Engine and Site Search. Google publishes a comparison table for
your reference.
  https://support.google.com/customsearch/answer/72326?hl=en

Google Site Search Simple (gss_simple) supports Google Site Search which is a
fee-based service. This module was built around the Custom Search Element
Control API and is integrated with core Search. It supports Drupal 7 only.
  http://developers.google.com/custom-search/docs/element

A search engine ID is required to use this module. To create or to access your
search engine ID, go to the Google Custom Search control panel.
  http://www.google.com/cse

For a full description of the module, visit the project page:
  http://www.drupal.org/sandbox/rojosnow/2297633

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/2297633


-- REQUIREMENTS --

* The core search module needs to be enabled.


-- INSTALLATION --

* Use the instructions to download the module.
    http://www.drupal.org/project/2297633/git-instructions

* Follow the normal Drupal module installation instructions. You'll need to
  upload/copy the module manually.
    https://www.drupal.org/documentation/install/modules-themes/modules-7

* Please note, the actual module name is "gss_simple" NOT
  "google_site_search_simple".

* Enable the module under Administration » Modules or
  use `drush en -y gss_simple`.


-- CONFIGURATION --

1) Change the permissions, if needed, to allow access for anonymous and/or
   authenticated users.

2) Under Administration » Configuration » Search and metadata » Search settings,
   enable Google Site Search Simple under Active Search Modules. Save
   your configuration.

3) Scroll down to the Google Site Search Simple settings. Enter your Google
   search engine ID.
     http://www.google.com/cse

4) Change the search path and/or search results tab if needed. Save
   your configuration.


-- CUSTOMIZATION --

* General Settings

  - You have the options of setting your path name for search and the
    search tab.

* Search Form Settings

  - These settings map specifically to the search form. You can enable/disable
    browser history for search and autocomplete.

  - To customize the search form, a wrapper class is provided to override the
    default Google styles.

* Search Block Form Settings

  - These settings map specifically to the search block form. You can
    enable/disable the search block form when displaying search results
    and autocomplete.

  - To customize the search block form, a wrapper class is provided to override
    the default Google styles.

* Search Results Settings

  - To customize the search results, a wrapper class is provided to override the
    default Google styles.


-- TROUBLESHOOTING --

* If the search forms or results do not display, check the following:

  - Make sure you clear the Drupal caches as well as Varnish.

  - Clear your browser caches and try again.


-- FAQ --

Q: Does this module work with Google Custom Search Engine (free version)?

A: Yes, this should work.


Q: Why doesn't my search box and results look like the theme settings in the
   CSE control panel?

A: Your Drupal theme is most likely overriding some styles provided by Google.
   Use the wrapper classes to override the elements that need additional
   styles provided.


-- CONTACT --

Current maintainers:
* Robert Jordan (rojosnow) - http://drupal.org/user/466858

This project has been sponsored by:
* RALLY SOFTWARE
  A leading agile and business agility software and services provider.
  Visit http://www.rallydev.com for more information.
