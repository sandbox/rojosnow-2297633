<?php

/**
 * @file
 * Displays the gss_simple search block form.
 *
 * Available variables:
 * - $search_block_form_class: string. The wrapper class for the search block
 *   form. Allows you to override Google supplied css.
 * - $enable_search_block_form_auto: boolean. Turn on/off autocomplete in search
 *   form.
 * - $results_url: string. The URL of the results page.
 *
 * @see template_preprocess_gss_simple_search_block_form()
 */
?>
<div class="<?php print $search_block_form_class; ?>">
  <div class="gcse-searchbox-only"
    data-enableAutoComplete="<?php print $enable_search_block_form_auto; ?>"
    data-queryParameterName="gss"
    data-resultsUrl="<?php print $results_url; ?>"></div>
</div>
