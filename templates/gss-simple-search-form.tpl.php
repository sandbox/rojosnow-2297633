<?php

/**
 * @file
 * Displays the gss_simple search form.
 *
 * Available variables:
 * - $search_form_class: string. The wrapper class for the search form. Allows
 *   you to override Google supplied css.
 * - $enable_search_form_auto: boolean. Turn on/off autocomplete in search
 *   form.
 * - $enable_search_form_history: boolean. Enable forward/back buttons in
 *   browser to retain search history.
 *
 * @see template_preprocess_gss_simple_search_form()
 */
?>
<div class="<?php print $search_form_class; ?>">
  <div class="gcse-searchbox"
    data-enableAutoComplete="<?php print $enable_search_form_auto; ?>"
    data-enableHistory="<?php print $enable_search_form_history; ?>"></div>
</div>
