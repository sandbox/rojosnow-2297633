<?php

/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * Available variables:
 * - $search_results_class: string. The wrapper class for the search results.
 *   Allows you to override Google supplied css.
 *
 * @see template_preprocess_gss_simple_search_results()
 */
?>
<div class="<?php print $search_results_class; ?>">
  <div class="gcse-searchresults"
    data-queryParameterName="gss"></div>
</div>
