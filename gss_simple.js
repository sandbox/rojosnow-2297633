/**
 * @file
 * Adds the Google Site Search javascript to the page.
 */

(function ($) {
  Drupal.behaviors.gssCx = {
    attach: function (context, settings) {
      $('body', context).once('gss', function () {
        var cx = Drupal.settings.gssCx;
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
            '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
      });
    }
  }
})(jQuery);
